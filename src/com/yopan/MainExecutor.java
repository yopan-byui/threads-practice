package com.yopan;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainExecutor {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        MainBehavior mb1 = new MainBehavior("Peter", 12, 1000);
        MainBehavior mb2 = new MainBehavior("John", 21, 500);
        MainBehavior mb3 = new MainBehavior("James", 27, 1500);
        MainBehavior mb4 = new MainBehavior("Mathew", 19, 2000);
        MainBehavior mb5 = new MainBehavior("Carl", 13, 2500);

        myService.execute(mb1);
        myService.execute(mb2);
        myService.execute(mb3);
        myService.execute(mb4);
        myService.execute(mb5);

        myService.shutdown();

    }
}

