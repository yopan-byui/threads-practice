package com.yopan;

import java.util.Random;

public class MainBehavior implements Runnable {

    private String name;
    private int number;
    private int sleep;
    private int randomNumber;

    public MainBehavior(String name, int number, int sleep) {
        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.randomNumber = random.nextInt(1000);
    }

    @Override
    public void run() {
        System.out.println("Class running for the player: " + name + ", who is " + number + "years old.");
        for(int count = 1; count < randomNumber; count++) {
            if(count % number == 0) {
                System.out.print(name + " is sleeping.");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println(name + "is done.");
    }
}
